FROM openjdk:11
WORKDIR /app
COPY src/main/java/org/example/Main.java /app/

RUN javac Main.java

ENTRYPOINT ["java","Main"]